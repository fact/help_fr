<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="nns_buildbayes" 
          xml:lang="fr"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>nns_buildbayes</refname>

    <refpurpose>construction d'un réseau de neurones à 1 couche cachée et avec biais </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Sequence d'appel</title>

    <synopsis>result=nns_buildbayes(wh_in,wo_in,x,y, (options))</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Paramètres</title>
     <variablelist>                        

        <varlistentry>   <term> wh_in: </term>   
             <listitem> <para>  coefficients des neurones cachés + biais, issus de l'initialisation       </para> 
			<para>  wh_in est une matrice de dimensions ((q+1) x nh) ou une structure Div       </para>
			<para>  nh est le nombre de neurons cachés       </para>
		</listitem>  </varlistentry>      

        <varlistentry>   <term> wo_in: </term>
             <listitem> <para> coefficients des neurones de sortie + biais, issus de l'initialisation  </para> 
			<para> wo_in est une matrice de dimensions ((nh+1) x no) ou une structure Div  </para>
			<para> no est le nombre de neurones de sortie  </para>
		</listitem>  </varlistentry>

       <varlistentry>   <term> x: </term>   
             <listitem> <para>  données d'étalonnage     </para> 
			<para>  x est une matrice de dimensions (n x q) ou une structure Div       </para>
		</listitem>  </varlistentry>      

     <varlistentry>   <term> y: </term>
             <listitem> <para> valeurs de référence, à prédire  </para> 
			<para> y est une matrice de dimensions (n x no) ou une structure Div  </para>
		</listitem>  </varlistentry>


    <varlistentry>   <term> (options_in): </term>
             <listitem>  
		 <para>options pour la construction du modèle </para> 
                 <para>options_in.maxtime: durée maximale d'exécution, en secondes (défaut=300)  						</para> 
                 <para>options_in.maxiter: nombre maximum d'itérations (défaut=10000)            						</para>
                 <para>options_in.displayfreq: écart entre deux itérations représentées dans une figure (défaut=10) 				</para>  
                 <para>options_in.precresid: précision des résidus; un scalaire ou vecteur de longueur no (défaut=1e-6*stdout) 			</para>
                 <para>options_in.precparam: précision de chacun des paramètres effectifs; un scalaire ou vecteur de longueur nc (défaut=1e-4)  </para> 
		 <para>options_in.stdresmin: minimum des écarts-types des résidus; un scalaire ou vecteur de longueur no (défaut=1e-6*stdout)	</para>                
		 <para>options_in.stdresmax: maximum des écarts-types des résidus; un scalaire ou vecteur de longueur no (défaut=0.1*stdout)    </para> 
		 <para>options_in.regclass: régularisation des poids  										</para> 
		 <para>… 0 = pas de régularisation  												</para> 
		 <para>… 1 = une classe pour tous les poids  											</para> 
		 <para>… 2 = deux classes: une pour les entrées, l'autre pour les sorties  							</para> 
		 <para>… 3 = une classe pour chaque entrée, une pour les biais de la couche cachée et une pour chaque sortie (par défaut) 	</para> 
		 <para>… 4 = une classe pour chaque poids  											</para> 
		 <para> options_in.preproc: prétraitement avant apprentissage 									</para> 
		 <para>… 0 = pas de prétraitement  												</para> 
		 <para>… 1 = normalisation (somme des carrés des écarts entre les entrées et les sorties égal à n)  				</para> 
		 <para>… 2 = standardisation (moyenne des entrées et sorties égale à 0, somme des carrés des écarts égale à n)			</para> 
		 <para> options_in.momentparam: ajustement du nombre effectif de paramètres dans chaque classe; entre 0 et 1 (défaut = 0.8)	</para> 
	       </listitem>  
    </varlistentry>

   <varlistentry>   <term> result.wh_out: </term>
             <listitem> <para> coefficients des neurones cachés + biais, après calcul du modèle  						</para> 
			<para> result.wh_out est une matrice de dimension ((q+1) x no) ou une structure Div  					</para>
			<para> nh est le nombre de neurones cachés 					  				</para>		</listitem>  </varlistentry>

   <varlistentry>   <term> result.wo_out: </term>
             <listitem> <para> coefficients des neurones de sortie + biais, après calcul du modèle 						</para> 
			<para> result.wo_out est une matrice de dimensions ((nh + 1) x no) ou une structure Div  				</para>
			<para> no est le nombre de neurones de sortie 						  				</para>
		</listitem>  </varlistentry>

   <varlistentry>   <term> result.stdres: </term>
             <listitem> <para> estimation de l'écart-type des résidus				 						</para> 
			<para> result.stdres est une structure Div  										</para>
			<para> result.stdres.d est un vecteur de dimensions (1 x no) 				  				</para>
		</listitem>  </varlistentry>

   <varlistentry>   <term> result.covw: </term>
             <listitem> <para> estimation de la matrice de variance-covariance des poids 	 						</para> 
			<para> result.covw est une structure Div  										</para>
			<para> result.covw.d est une matrice de dimensions (nw x nw)				  				</para>
		</listitem>  </varlistentry>

   <varlistentry>   <term> result.options_out: </term>
             <listitem>  
		 <para>options_in avec en plus les champs suivants: </para> 
                 <para>result.options_out.stop: une chaine de caractères indiquant la cause de l'arrêt de l'apprentissage			</para> 
                 <para>result.options_out.r2: coefficient de détermination pour chaque sortie          						</para>
                 <para>result.options_out.wheff: rapport effectif/total du nombre de paramètres pour chaque poids de la couche cachée 		</para>  
                 <para>result.options_out.wheff.d est une matrices de dimensions ((q+1) x nh)				 			</para>
                 <para>result.options_out.woeff: rapport effectif/total du nombre de paramètres pour chaque poids de la couche de sortie	</para> 
		 <para>result.options_out.woeff.d est une matrices de dimensions ((nh+1) x no)							</para>                
		 <para>result.options_out.histiters: nombre d'itérations								      	</para> 
		 <para>result.options_out.histresid: erreurs résiduelles pour chaque itération  						</para>
		 <para>result.options_out.histresid.d est une matrice de no lignes et autant de colonnes que d'itérations 			</para> 
		 <para>result.options_out.histparam: nombre effectif de paramètres, pour chaque classe						</para> 
		 <para>result.options_out.histparam.d a une colonne par classe, et autant de lignes que d'itérations				</para> 
		 <para>result.options_out.classnumbers: numéros de classe de chaque poids, classés dans un vecteur selon [wh(:);wo(:)]		</para> 
		 <para>result.options_out.totalparam: nombre total de paramètres (poids) dans chaque classe de poids 			 	</para> 
		 <para>result.options_out.totalparam.d est un vecteur-ligne avec autant de colonnes que de classes				</para> 
	       </listitem>  
    </varlistentry> 
  
      </variablelist>            
  </refsection>

  <refsection>
    <title>Exemples</title>    
    <programlisting role="example">[res]=nns_buildbayes(wh,wo,x,y)                                  </programlisting>  
  </refsection> 
  
  <refsection>
    <title>Bibliographie</title>
     <simplelist type="vert">
      <member>  </member>
     </simplelist>
  </refsection>

  <refsection>
    <title>Auteurs</title>
    <simplelist type="vert">
      <member>IC Trelea, AgroParisTech   </member>
    </simplelist>
  </refsection>
  
</refentry>




