<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="vodka" 
          xml:lang="fr"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>vodka</refname>

    <refpurpose> régression Vodka-PLSR  </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Sequence d'appel</title>

    <synopsis>model=vodka(x,y,split,lv,r,centred)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Paramètres</title>
     <variablelist>                        

        <varlistentry>   <term> x et y : </term>   
             <listitem> <para>  données d'étalonnage; une matrice (n x q) et un vecteur (n x 1) ou des structures Div   </para> </listitem>  </varlistentry>   

            <varlistentry>   <term> split: </term>
             <listitem> <para> paramètre pour la validation croisée:  </para> 
          <para>  - un entier:  nombre de blocs tirés au hasard  </para> 
        <para>  - deux entiers: [a b ]: a blocs tirés au hasard; b répétitions </para>
         <para>  - un vecteur de dimension n attribuant chaque échantillon à un bloc (nombres 1,2,...k pour k blocs): blocs fixés par le vecteur </para>  
        <para>  - une matrice (n x b ) de vecteurs-colonne de dimension n attribuant chaque échantillon à un bloc (nombres 1,2,...k pour k blocs): blocs fixés par chaque vecteur, b répétitions   </para>
         <para>  - 'vnbxx':  stores vénitiens, xx blocs; ex: 'vnb10' pour 10 blocs  </para> 
         <para>  - 'jckxx':  Jack knife, xx blocs; ex: 'jck8' pour 8 blocs  </para> 
        </listitem>                  </varlistentry>                

        <varlistentry>   <term> lv : </term>
             <listitem> <para> nombre de variables latentes                        </para> </listitem>  </varlistentry>

        <varlistentry>   <term> r : </term>
             <listitem>
                 <para>  paramètre pour Vodka; un vecteur (q x 1) ou un entier parmi {0,1,2}:       </para>
                 <simplelist type="vert">                       
                        <member> r=0 -->  le vecteur moyenne des colonnes de x                      </member>
                        <member> r=1 -->  x'.y soit l'équivalent de la PLSR standard          </member>
                        <member> r=2 -->  x'.y2 avec y2 contenant les valeurs de y au carré         </member>   
                 </simplelist> </listitem>  </varlistentry>
 
        <varlistentry>  <term> (centred): </term> 
             <listitem> <para> centrage=1 (par défaut); pas de centrage=0            </para> </listitem> </varlistentry>

        <varlistentry>   <term> model.err: </term>
           <listitem> <para> les erreurs standard d'étalonnage et de validation croisée                 </para> 
                      <para> model.err.d est une matrice (lv x 2); les colonnes sont les rmsec et rmsecv respectivement                               </para> </listitem> </varlistentry>

       <varlistentry>   <term> model.ypredcv: </term>
             <listitem> <para>les valeurs y prédites après validation croisée                   </para>     
             <para> model.ypredcv.d est une matrice (n x lv)                                    </para> </listitem> </varlistentry>

       <varlistentry>   <term> model.b: </term>
             <listitem> <para>les b ou coefficients de régression                               </para>                           
             <para> model.b.d est une matrice (q x lv)                                          </para> </listitem> </varlistentry>   

       <varlistentry>   <term> model.scores: </term>
             <listitem> <para>les coordonnées des observations sur les loadings    </para>                      
             <para> model.scores.d est une matrice de dimensions (n x lv)                                          </para> </listitem> </varlistentry>
             
       <varlistentry>   <term> model.loadings: </term>
             <listitem> <para>les loadings       </para>                           
             <para> model.loadings.d est une matrice de dimensions (q x lv)                                          </para> </listitem> </varlistentry>

       <varlistentry>   <term> model.x_mean, model.y_mean: </term>      
             <listitem> <para>moyennes de x et y , un vecteur (q x 1) et un scalaire         </para> </listitem> </varlistentry> 
 
       <varlistentry>   <term> model.center: </term>
             <listitem> <para>1=centré; 0=non centré  </para> </listitem> </varlistentry>
 
       <varlistentry>   <term> rmsec: </term>      
             <listitem> <para>rmsec=model.rmsec.d                                                     </para> </listitem> </varlistentry> 
 
       <varlistentry>   <term> rmsecv: </term>      
             <listitem> <para>rmsecv=model.rmsecv.d                                                    </para> </listitem> </varlistentry> 
 
       <varlistentry>   <term> b: </term>      
             <listitem> <para>b=model.b.d                                                         </para> </listitem> </varlistentry> 
 
       <varlistentry>   <term> ypredcv: </term>      
             <listitem> <para>ypredcv=model.ypredcv.d                                                   </para> </listitem> </varlistentry> 
 
             
      </variablelist>            
  </refsection>

  <refsection>
    <title>Exemples</title>    
    <programlisting role="example">[model]=vodka(x,y,50,20,1)                                </programlisting>
    <programlisting role="example">[model]=vodka(x,y,50,20,1,0)                              </programlisting>   
    <programlisting role="example">[model]=vodka(x,y,50,20,mean(x,'r'),0)                    </programlisting>
  </refsection> 
  
  <refsection>
    <title>Bibliographie</title>
     <simplelist type="vert">
      <member>Boulet et al., A family of regression methods derived from standard PLSR, Chemom.Intell.Lab.Syst.,2012 </member>
     </simplelist>
  </refsection>

  <refsection>
    <title>Auteurs</title>
    <simplelist type="vert">
      <member>JC Boulet, INRA   </member>
      <member>JM Roger, IRSTEA  </member>
    </simplelist>
  </refsection>
  
</refentry>
