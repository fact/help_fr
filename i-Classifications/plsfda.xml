<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="plsfda"
          xml:lang="fr"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>plsfda</refname>

<refpurpose> analyse discriminante PLS avec validation croisée: une PLS2 (Simpls) calcule des scores sur la matrice disjonctive des classes, puis une Analyse Factorielle Discriminante est appliquée sur ces scores, produisant de nouveaux scores; chaque observation est attribuée à une classe en utilisant ces nouveaux scores  </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Séquence d'appel</title>

    <synopsis>model = plsfda(x,y_class,split,lv,(metric),(scale),(threshold)) </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Paramètres</title>
    <variablelist>
      <varlistentry>
         <term> x: </term>   
             <listitem> <para> une matrice (n x q) ou une structure Div 
             </para> </listitem> </varlistentry>       

      <varlistentry>
        <term> y_class: </term>
          <listitem> <para> un vecteur conjonctif (n x 1) ou une matrice disjonctive (n x nclass) ou une structure Div  </para>            </listitem> </varlistentry>  
          
      <varlistentry> 
        <term> split: </term> 
          <listitem> <para> pour la validation croisée; le nombre de blocs, ou un vecteur de dimensions (n x 1) identifant par un numéro le groupe de validation croisée de chaque observation    </para> 
          </listitem> </varlistentry>       

 
       <varlistentry>
         <term> lv: </term>   
          <listitem> <para> les nombres de dimensions utilisés pour la PLS2 et la FDA </para>
              <para> lv est un vecteur de longueur 2; lv(1) est la dimension maximale de la PLS2, lv(2) est la dimension maximale de la FDA; lv(2) devrait être inférieur ou égal à nclass-1  </para>
             </listitem> </varlistentry>  
   
       <varlistentry>
         <term> (metric): </term>   
           <listitem> <para> le choix de la métrique utilisée pour mesurer la distance des coordonnées (scores) d'un individu à une classe  </para>
             <para> metric=0:  distance de Mahalanobis (par défaut) </para>
             <para> metric=1:  distance Euclidienne usuelle   </para>
             </listitem> </varlistentry> 
   
       <varlistentry>
         <term> (scale): </term>   
             <listitem> <para> les colonnes de x sont toujours centrées; scale définit la standardisation  </para>
             <para> scale='c':  centrage </para>
             <para> scale='cs':  standardisation: division des colonnes de x par leur écart-type (par défaut)   </para>
             </listitem> </varlistentry>
   
       <varlistentry>
         <term> (threshold): </term>   
             <listitem> <para> le seuil d'affectation à une classe; par défaut 1/nclass   </para>
             </listitem> </varlistentry> 

        <varlistentry>
            <term> model.conf_cal_nobs: </term>
            <listitem> <para> les matrices de confusion en étalonnage, exprimées en nombre d'observations </para>
                <para> model.conf_cal_nobs est une liste de lv objets, chacun étant de dimensions (nclass x nclass) </para>
            </listitem> </varlistentry>
        
        
       <varlistentry>
         <term> model.conf_cal: </term>   
             <listitem> <para> les matrices de confusion en étalonnage, exprimées en pourcentage  </para>
             <para> model.conf_cal est une liste de lv objets, chacun étant de dimensions (nclass x nclass) </para>
             </listitem> </varlistentry>  
 
       <varlistentry>
         <term> model.conf_cv: </term>   
             <listitem> <para> les matrices de confusion en validation croisée, exprimées en pourcentage  </para>
             <para> model.conf_cv est une liste de lv objets, chacun étant de dimensions (nclass x nclass) </para>
             </listitem> </varlistentry>   
   
       <varlistentry>
         <term> model.err: </term>   
             <listitem> <para> les pourcentages d'erreur de classification des étalonnages (1° colonne) et de la validation croisée (2° colonne) </para>
             <para> model.err.d est une matrice (lv x 2)</para> 
             </listitem> </varlistentry>   

       <varlistentry>
         <term> model.errbycl_cal: </term>   
             <listitem> <para> le pourcentage d'erreur de l'étalonnage, pour chaque classe  </para>
             <para> model.errbycl_cal.d est une matrice (lv x nclass) </para> 
             </listitem> </varlistentry>   
   
        <varlistentry>
         <term> model.errbycl_cv: </term>   
             <listitem> <para> le pourcentage d'erreur de la validation croisée, pour chaque classe  </para>
             <para> model.errbycl_cv.d est une matrice (lv x nclass) </para> 
             </listitem> </varlistentry> 

        <varlistentry>
         <term> model.notclassed: </term>   
             <listitem> <para> le pourcentages d'échantillons non classés (prédictions inférieures au seuil) </para>
             <para> model.notclassed.d est un vecteur (lv x 1) </para> 
             </listitem> </varlistentry>   
   
        <varlistentry>
         <term> model.notclassed_bycl: </term>   
             <listitem> <para> le pourcentages d'échantillons non classés (prédictions inférieures au seuil), pour chaque classe  </para>
             <para> model.notclassed_bycl.d est une matrice (lv x nclass) </para> 
             </listitem> </varlistentry> 
 
        <varlistentry>
         <term> model.method: </term>   
             <listitem> <para> la méthode utilisée pour l'analyse discriminante; ici: 'plsda' </para>
             </listitem> </varlistentry> 

        <varlistentry>
            <term> model.xcal: </term>
            <listitem> <para> le jeu d'étalonnage; une structure div </para>
             <para> model.xcal.d est de dimensions (n x q) </para>
            </listitem> </varlistentry>
        
        <varlistentry>
            <term> model.ycal: </term>
            <listitem> <para> les classes du jeu d'étalonnage, sous forme disjonctive; une structure div</para>
                <para> model.ycal.d est de dimensions (n x nclass) </para>
            </listitem> </varlistentry>
 
        <varlistentry>
            <term> model.scores: </term>
            <listitem> <para> les scores des observations issus de Simpls; une structure Div </para>
                <para> model.scores.d est de dimensions (n x lv) </para>
            </listitem> </varlistentry>
        
         <varlistentry>
         <term> model.rloadings: </term>
             <listitem> <para> la matrice R de Simpls, vérifiant: T=XR</para>
             <para> model.rloadings.d est une matrice de dimensions (q x lv) </para>
             </listitem> </varlistentry> 
        
         <varlistentry>
         <term> model.classif_metric: </term>   
           <listitem> <para> la métrique utilisée sur les scores des individus pour le calcul des distances entre individus et groupes </para>
             </listitem> </varlistentry> 
 
         <varlistentry>
         <term> model.scale: </term>   
           <listitem> <para> la standardisation appliquée </para>
             </listitem> </varlistentry>   
       
        <varlistentry>
         <term> model.threshold: </term>   
           <listitem> <para> le seuil appliqué pour l'appartenance à une classe  </para>
             </listitem> </varlistentry> 
 
 
           
     </variablelist>                 
  </refsection>

  <refsection>
    <title>Exemples</title>    
    <programlisting role="example">[result1]=plsfda(x,y,30,[20,6])                 </programlisting>
    <programlisting role="example">[result1]=plsfda(x,y,30,[20,6],1,'cs',0.01)        </programlisting>
  </refsection>


  <refsection>
    <title>Auteurs</title>
    <simplelist type="vert">
      <member>JM Roger, IRSTEA  </member>
      <member>JC Boulet, INRA   </member>
    </simplelist>
  </refsection>
  
</refentry>
